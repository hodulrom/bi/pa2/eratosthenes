# STL & Eratosthenes
Assignment of the 7th laboratory. This program implements an integer container capable of finding an element occurrences count in logarithmic time complexity and all duplicates in linear time complexity using STL containers.

As an addition, the [Eratosthenes Sieve](https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes) algorithm that uses STL's list container to store prime numbers is implemented as well.

This program does not accept any input nor prints any output, but tests are implemented so those can be run (see [usage](#Usage)).

[EDUX](https://edux.fit.cvut.cz/courses/BI-PA2/teacher/peckato1/tut07)

## Usage

### Compile
```bash
make
```

### Test
```bash
make tests
```
