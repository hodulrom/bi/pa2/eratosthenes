all:
	mkdir -p bin
	g++ -Wall -pedantic -std=c++11 main.cpp -o bin/test.out
	cp primes.txt bin/primes.txt

tests:
	bin/test.out

clean:
	rm -rf bin/
