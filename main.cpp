#include<iostream>
#include<vector>
#include<algorithm>
#include<map>
#include<cstdlib>
#include<list>
#include <cmath>
#include <fstream>
#include <cassert>
#include <climits>
#include <sstream>

using namespace std;

/**
 * Functor sorting integers in descending order.
 */
struct DescendingComparator {
	bool operator()(int a, int b) const {
		return b < a;
	}
};

class IntegerContainer {
private:
	vector<int> elements;
	map<int, int, DescendingComparator> occurrences;

public:

	/**
	 * Inserts the value into the container.
	 *
	 * @param value Element to insert.
	 * @return Reference to this instance.
	 */
	IntegerContainer & insert(int value) {
		this->elements.push_back(value);
		const auto & it = this->occurrences.find(value);

		if (it == this->occurrences.end()) {
			this->occurrences.insert(std::make_pair(value, 1));
		}
		else {
			++it->second;
		}

		return * this;
	}

	/**
	 * Finds count of occurrences of given number within the container.
	 *
	 * @param value Number to find the occurrences of.
	 * @return Occurrence count.
	 */
	int count(int value) const {
		const auto & it = this->occurrences.find(value);

		if (it == this->occurrences.end()) {
			return 0;
		}

		return it->second;
	}

	/**
	 * Prints numbers that are present more than once in the container in format "num: count" per line.
	 *
	 * @param os Output stream.
	 * @return Given output stream.
	 */
	ostream & printCollisions(ostream & os) const {
		for (const auto & occurence : this->occurrences) {
			if (occurence.second > 1) {
				os << occurence.first << ": " << occurence.second << endl;
			}
		}

		return os;
	}

	/**
	 * Finds the closest number to n within the container that is greater or equal to it.
	 *
	 * @param n Number to find the upper bound of.
	 * @return Upper bound or, if the number is not present in the container, INT_MAX.
	 */
	int upperBound(int n) {
		auto it = --this->occurrences.upper_bound(n);

		if (it == this->occurrences.end()) {
			return INT_MAX;
		}

		return it->first;
	}

	/**
	 * Finds the closest number to n within the container that is less or equal to it.
	 *
	 * @param n Number to find the upper bound of.
	 * @return Lower bound or, if the number is not present in the container, INT_MIN.
	 */
	int lowerBound(int n) {
		auto it = this->occurrences.lower_bound(n);

		if (it == this->occurrences.end()) {
			return INT_MIN;
		}

		return it->first;
	}

	int operator[](int i) const {
		return this->elements[i];
	}

	friend ostream & operator<<(ostream & os, const IntegerContainer & c) {
		for (const int & value : c.elements) {
			os << value << ' ';
		}

		return os;
	}
};

/**
 * Returns list of prime numbers up to n.
 *
 * @param n Upper bound.
 * @return Prime numbers up to n.
 */
list<int> eratosthenes(int n) {
	list<int> numbers;
	int nSqrt = (int) ceil(sqrt(n));

	for (int i = 2; i <= n; ++i) {
		numbers.push_back(i);
	}

	for (auto it = numbers.begin(); it != numbers.end() && * it < nSqrt; ++it) {
		int multiple = * it;

		numbers.remove_if([multiple](int i) {
			return i > multiple && i % multiple == 0;
		});
	}

	return numbers;
}

int main() {
	stringstream ss;
	IntegerContainer c;
	int someNums[] = {1, 3, 52, 13, 85, 63, 666, 8, 4, 8, 666};

	for (int n : someNums) {
		c.insert(n);
	}

	assert(c.upperBound(-5000) == 1);
	assert(c.upperBound(0) == 1);
	assert(c.upperBound(1) == 1);
	assert(c.upperBound(2) == 3);
	assert(c.upperBound(51) == 52);
	assert(c.upperBound(53) == 63);
	assert(c.upperBound(52) == 52);
	assert(c.upperBound(420) == 666);
	assert(c.upperBound(999) == INT_MAX);

	assert(c.lowerBound(-5000) == INT_MIN);
	assert(c.lowerBound(0) == INT_MIN);
	assert(c.lowerBound(1) == 1);
	assert(c.lowerBound(2) == 1);
	assert(c.lowerBound(51) == 13);
	assert(c.lowerBound(53) == 52);
	assert(c.lowerBound(52) == 52);
	assert(c.lowerBound(420) == 85);
	assert(c.lowerBound(999) == 666);

	assert(c.count(666) == 2);
	assert(c.count(69) == 0);
	assert(c.count(1) == 1);
	assert(c.count(3) == 1);
	assert(c.count(52) == 1);
	assert(c.count(13) == 1);
	assert(c.count(85) == 1);
	assert(c.count(63) == 1);
	assert(c.count(8) == 2);
	assert(c.count(63) == 1);
	assert(c.count(4) == 1);
	assert(c.count(666) == 2);
	assert(c.count(INT_MAX) == 0);
	assert(c.count(INT_MIN) == 0);

	for (int i = 0; i < 11; ++i) {
		assert(c[i] == someNums[i]);
	}

	c.printCollisions(ss);
	assert(ss.str() == "666: 2\n8: 2\n");

	ifstream primesRef("primes.txt");
	list<int> primes = eratosthenes(500000);

	for (auto i : primes) {
		int ref;

		assert(primesRef.good());
		primesRef >> ref;
		assert(ref == i);
	}
}
